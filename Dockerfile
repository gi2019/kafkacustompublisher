FROM python:3.6.1-alpine

WORKDIR /home/app

ADD . /home/app

RUN pip install -r requirements.txt
CMD ["python","main.py"]