import json
from kafka import KafkaProducer


class Producer:

    def __init__(self, cluster_address, topic):
        self.cluster_address = cluster_address
        self.topic = topic
        self.status = 'Disconnected'
        self.producer = self.connect_kafka_producer(self.cluster_address)

    def connect_kafka_producer(self, cluster_address):
        _producer = None
        try:
            _producer = KafkaProducer(bootstrap_servers=[cluster_address], api_version=(0, 10))
            self.status = f"Successfully connected to bootstrap-servers at {cluster_address}"
        except Exception as ex:
            print(f"Exception occurred while connecting to Kafka cluster {ex}")
        finally:
            return _producer

    def publish_message(self, key, value, num=1):
        """
        :param key: Key of a Kafka topic
        :param value: Message body
        :param num: Number of iterations to send the message
        :return:
        """
        try:
            key_bytes = bytes(key, encoding='utf8')
            value_bytes = bytes(value, encoding='utf8')
            for i in range(num):
                self.producer.send(self.topic, key=key_bytes, value=value_bytes)
                self.producer.flush()
                print(f"Published msg {value}")
        except Exception as ex:
            print(f"Exception occurred while publishing to Kafka cluster {ex}")

