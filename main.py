import json

from datetime import datetime

from flask import Flask, render_template, request

from Producer import Producer


app = Flask(__name__)

# For Debug purposes
KAFKA_URL = '10.0.0.243:9092'
KAFDROP_BASE_URL = '10.0.0.243:9003'
TOPIC = 'HUY'


@app.route('/')
def home():
    return render_template('index.html', status=producer.status, time=formattedDate)


@app.route('/publish', methods=['POST'])
def publish():
    try:
        topic = request.form['TopicInput']
        message = request.form['messageInput']
        num_messages = int(request.form['numMessagesInput'])
        print(topic, message, num_messages)
        producer.topic = topic
        producer.publish_message('parsed', message, num_messages)
        return render_template('res.html', topic=producer.topic,
                               address=KAFDROP_BASE_URL)
    except Exception as ex:
        return json.dumps({'status': 'error occurred', 'exception': str(ex)})


@app.route('/api/publish', methods=['POST'])
def api_publish() -> json:
    try:
        req_data = request.get_json()
        topic = req_data['topic']
        message = req_data['message']
        num_messages = int(req_data['num_messages'])
        producer.topic = topic
        producer.publish_message('parsed', message, num_messages)
        return json.dumps({'code': 200, 'message': message})
    except Exception as ex:
        return json.dumps({'status': 'error occurred', 'exception': str(ex)})


if __name__ == "__main__":
    producer = Producer(KAFKA_URL, TOPIC)
    now = datetime.now()
    formattedDate = now.strftime("%d/%m/%Y %H:%M:%S")
    app.run(host="0.0.0.0", port=8000, debug=True)



